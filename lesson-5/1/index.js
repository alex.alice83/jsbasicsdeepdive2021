/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

// РЕШЕНИЕ

const f2 = function (num) {
  if (Number.isNaN(num) || typeof num !== 'number') {
    throw new Error('Not a number was input');
  } else {
    return Math.pow(num, 3);
  }
};
// console.log(f2('string')); // 8
console.log(f2(2)); // 8
console.log(f2(10)); // 1000
