/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ

validate = function (num) {
  if (Number.isNaN(num) || typeof num !== 'number') {
    throw new Error('Not a number was input');
  }
};

const f = function (...args) {
  let arrayFormArgs = args;

  for (item of arrayFormArgs) {
    validate(item);
  }

  let sumTotal = arrayFormArgs.reduce((sum, current) => {
    return sum + current;
  }, 0);

  return sumTotal;
};

console.log(f(1, 1, 1, 2, 1, 1, 1, 1)); // 9
