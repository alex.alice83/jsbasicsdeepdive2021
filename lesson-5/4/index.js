/**
 * Задача 7.
 *
 * Создайте функцию `getDivisors`, которая принимает число в качестве аргумента.
 * Функция возвращает массив его делителей (чисел, на которое делится данное число начиная от 1 и заканчивая самим собой).
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 * - Генерировать ошибку, если в качестве входного аргумента был передано число меньшее, чем 1.
 *
 * Заметки:
 * - В решении вам понадобится использовать цикл с перебором массива.
 */

// РЕШЕНИЕ

validate = function (num) {
  if (Number.isNaN(num) || typeof num !== 'number') {
    throw new Error('Not a number was input');
  } else if (num < 1) {
    throw new Error('The argument less then 1 was pass');
  }
};

getDivisors = function (num) {
  validate(num);
  let dividersArray = [];
  for (let i = 1; i <= num; i++) {
    if (num % i === 0) {
      dividersArray.push(i);
    }
  }
  return dividersArray;
};

getDivisors(12); // [1, 2, 3, 4, 6, 12]
