/**
 * Задача 6.
 *
 * Создайте функцию `isEven`, которая принимает число качестве аргумента.
 * Функция возвращает булевое значение.
 * Если число, переданное в аргументе чётное — возвращается true.
 * Если число, переданное в аргументе нечётное — возвращается false.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 */

// РЕШЕНИЕ

validate = function (num) {
  if (Number.isNaN(num) || typeof num !== 'number') {
    throw new Error('Not a number was input');
  }
};

isEven = function (num) {
  validate(num);

  if (num % 2 === 0) {
    return true;
  } else {
    return false;
  }
};

isEven(3); // false
isEven(4); // true

console.log(isEven(3));
console.log(isEven(4));
