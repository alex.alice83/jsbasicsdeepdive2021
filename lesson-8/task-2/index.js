/**
 * Доработать форму из 1-го задания.
 *
 * Добавить обработчик сабмита формы.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
 */

// РЕШЕНИЕ

import {
  divEmailInput,
  divPassInput,
  divCheckInput,
  btnSubmit,
} from './loginForm.js';

const isEmptyOrSpaces = function (strInput) {
  return strInput === null || strInput.match(/^ *$/) !== null;
};

btnSubmit.addEventListener('click', (e) => {
  e.preventDefault();

  let emailInputValue = divEmailInput.value;
  let passInputValue = divPassInput.value;
  let checkInputValue = divCheckInput.checked;

  if (isEmptyOrSpaces(emailInputValue) || isEmptyOrSpaces(passInputValue)) {
    throw new Error(
      'Error. the form tried to be submitted with empty strings or white spaces.'
    );
  }

  let outputObj = {
    email: emailInputValue,
    password: passInputValue,
    remember: checkInputValue,
  };

  console.log(outputObj);

  return;
});
