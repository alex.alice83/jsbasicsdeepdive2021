/**
 * Создать форму динамически при помощи JavaScript.
 *
 * В html находится пример формы которая должна быть сгенерирована.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
 */

// РЕШЕНИЕ

//Find form master div
const form = document.getElementById('form');

//Create elements

//Email group
const divEmail = document.createElement('div');
divEmail.setAttribute('class', 'form-group');
const divEmailLabel = document.createElement('label');
divEmailLabel.setAttribute('for', 'email');
divEmail.appendChild(divEmailLabel);
const labelEmailText = document.createTextNode('Электропочта');
divEmailLabel.appendChild(labelEmailText);

const divEmailInput = document.createElement('input');
divEmailInput.setAttribute('type', 'email');
divEmailInput.setAttribute('class', 'form-control');
divEmailInput.setAttribute('id', 'email');
divEmailInput.setAttribute('placeholder', 'Введите свою электропочту');
divEmailInput.setAttribute('autocomplete', 'current-email');
divEmail.appendChild(divEmailInput);

//Pass group
const divPass = document.createElement('div');
divPass.setAttribute('class', 'form-group');
const divPassLabel = document.createElement('label');
divPassLabel.setAttribute('for', 'password');
divPass.appendChild(divPassLabel);
const labelPassText = document.createTextNode('Пароль');
divPassLabel.appendChild(labelPassText);

const divPassInput = document.createElement('input');
divPassInput.setAttribute('type', 'password');
divPassInput.setAttribute('class', 'form-control');
divPassInput.setAttribute('id', 'password');
divPassInput.setAttribute('placeholder', 'Введите пароль');
divPassInput.setAttribute('autocomplete', 'current-password');
divPass.appendChild(divPassInput);

//CheckboxGroup
const divCheck = document.createElement('div');
divCheck.setAttribute('class', 'form-group form-check');

const divCheckInput = document.createElement('input');
divCheckInput.setAttribute('type', 'checkbox');
divCheckInput.setAttribute('class', 'form-check-input');
divCheckInput.setAttribute('id', 'exampleCheck1');
divCheck.appendChild(divCheckInput);

const divCheckLabel = document.createElement('label');
divCheckLabel.setAttribute('class', 'form-check-label');
divCheckLabel.setAttribute('for', 'exampleCheck1');
divCheck.appendChild(divCheckLabel);
const labelCheck = document.createTextNode('Запомнить меня');
divCheckLabel.appendChild(labelCheck);

//Button submit
const btnSubmit = document.createElement('button');
const btnText = document.createTextNode('Вход');
btnSubmit.appendChild(btnText);
btnSubmit.setAttribute('type', 'submit');
btnSubmit.classList.add('btn');
btnSubmit.classList.add('btn-primary');

//Adding of elements to fragment
const fragment = document.createDocumentFragment();

fragment.appendChild(divEmail);
fragment.appendChild(divPass);
fragment.appendChild(divCheck);
fragment.appendChild(btnSubmit);

//Adding elements to main from
form.appendChild(fragment);
