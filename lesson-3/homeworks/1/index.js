/*
 * Задача 1.
 *
 * Дописать требуемый функционал что бы код работал правильно.
 * 
 * Необходимо преобразовать первый символ строки в заглавный и вывести эту строку в консоль.
 * Пример 1: привет -> Привет
 * 
 * Пример 2: 222 -> Error.
 *
 * Условия:
 * - Необходимо проверить что введенный текст не число иначе выводить ошибку в консоль.
 */

const str = prompt('Введите любую строку:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// РЕШЕНИЕ
const strParsedCheck = Number(str);
if (Number.isNaN(strParsedCheck)) {
    // alert(`string`)
    let tempString = str;
    let strFirstLetterToUpperCase = tempString[0].toUpperCase();
    let tempStringSliced = tempString.slice(1);
    let capitalizedConcatedString = strFirstLetterToUpperCase + tempStringSliced;
    console.log(capitalizedConcatedString);
} else {
    console.log(`Error. A number was input.Please input string instead.`);
}