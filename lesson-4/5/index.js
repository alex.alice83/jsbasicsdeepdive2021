/**
 * Задача 5.
 *
 * Написать скрипт, который возвращает массив с объектами - характеристиками элементов массива.
 * Объекты должен иметь такие свойства:
 *      value - значение элемента;
 *      type - тип данных элемента.
 *
 * Если тип данных элемента СТРОКА объекты должны иметь дополнительное свойство length.
 * Если тип данных элемента не ЧИСЛО и не СТРОКА объекты должны иметь дополнительное
 * свойство error с текстом 'Invalid type'.
 *
 * Пример: arr -> ['Доброе утро!', 2, {}]
 *         objArr = [
 *              { value: 'Доброе утро!', type: 'string', length: 12 },
 *              { value: 2, type: 'number' },
 *              { value: {}, type: 'object', error: 'Invalid type' }
 *         ];
 *
 * Условия:
 *  - Обязательно использовать встроенный метод массива map.
 */

const array = [
  'Доброе утро!',
  null,
  2,
  'Привет',
  NaN,
  () => {},
  [],
  'Добрый вечер!',
  {},
  'ананас',
  '#',
  'До свидания!',
]; // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// РЕШЕНИЕ

let arrayStackMain = [];

const arrayOfObjects = array.map((item) => {
  let type1 = `type`;
  let type2 = `${typeof item}`;
  let value1 = `value`;
  let value2 = item;
  let length1 = `length`;
  let error1 = `error`;
  let error2 = `Invalid type`;

  let tempArrayType = [];
  let tempArrayValue = [];
  let tempArrayLength = [];
  let tempArrayError = [];
  let tempArrayStack = [];

  tempArrayType.push(type1);
  if (item === null) {
    type2 = null;
    tempArrayType.push(type2);
  } else {
    tempArrayType.push(type2);
  }

  tempArrayValue.push(value1);
  tempArrayValue.push(value2);

  tempArrayStack.push(tempArrayValue);
  tempArrayStack.push(tempArrayType);

  if (typeof item === 'string') {
    length2 = item.length;
    tempArrayLength.push(length1);
    tempArrayLength.push(length2);
    tempArrayStack.push(tempArrayLength);
  } else if (
    (!(typeof item === 'string') && !(typeof item === 'number')) ||
    Number.isNaN(item)
  ) {
    tempArrayError.push(error1);
    tempArrayError.push(error2);
    tempArrayStack.push(tempArrayError);
  }

  arrayStackMain.push(tempArrayStack);
});

let newArray = [];

arrayStackMain.forEach((item, index) => {
  let objFromArray = Object.fromEntries(arrayStackMain[index]);
  newArray.push(objFromArray);
});

console.log(newArray);
