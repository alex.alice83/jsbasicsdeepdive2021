/**
 * Задача 2.
 *
 * Напишите скрипт, который проверяет идентичны ли массивы.
 * Если массивы полностью идентичны - в переменную flag присвоить значение true,
 * иначе - false.
 *
 * Пример 1: const arr1 = [1, 2, 3];
 *           const arr2 = [1, '2', 3];
 *           flag -> false
 *
 * Пример 2: const arr1 = [1, 2, 3];
 *           const arr2 = [1, 2, 3];
 *           flag -> true
 *
 * Пример 3: const arr1 = [];
 *           const arr2 = arr1;
 *           flag -> true
 *
 * Пример 4: const arr1 = [];
 *           const arr2 = [];
 *           flag -> true
 *
 * Условия:
 * - Обязательно проверять являются ли сравниваемые структуры массивами;
 *
 */

const arr1 = [1, 2, 3];
const arr2 = [1, 2, 3];
let flag = '';

// РЕШЕНИЕ

const compareIdenticalArray = (arr1, arr2) => {
  if (
    Array.isArray(arr1) &&
    Array.isArray(arr2) &&
    arr1.length === arr2.length
  ) {
    if (arr1.length === 0) {
      flag = true;
    } else {
      for (let i = 0; i < arr1.length; i++) {
        if (arr1[i] === arr2[i]) {
          flag = true;
        } else {
          flag = false;
        }
      }
    }
  } else {
    console.log(
      `Error. Not an array was passed as argument or arrays have different lengths`
    );
  }
  return flag;
};

compareIdenticalArray(arr1, arr2);
console.log(flag);
