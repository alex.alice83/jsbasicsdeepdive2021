// Перепишите `if..else` с использованием нескольких операторов `?`. 
// Для читаемости — оформляйте код в несколько строк.
"use strict"

////////////////// Задание //////////////////
// let message;

// if (login === 'Pitter') {
//   message = 'Hi';
// } else if (login === 'Owner') {
//   message = 'Hello';
// } else if (login === '') {
//   message = 'unknown';
// } else {
//   message = '';
// }

////////////////// Решение //////////////////
let login;
// login = 'Pitter';
login = 'Owner';
// login = '';
// login = undefined;


let message = login === 'Pitter' ? 'Hi' :
    login === 'Owner' ? 'Hello' :
    login === '' ? 'unknown' :
    '';

console.log(message);